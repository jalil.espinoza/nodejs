//1. Otra manera desarrollar el fibonacci
/*
const prompt=require("prompt-sync")();
console.clear();
console.log("App para imprimir la secuencia de fibonacci \nDesarrollado por: Jalil Espinoza");
var a = 0, b = 1, c = 0, limite = 0;

limite = prompt("Escriba el limite de la serie Fibonacci: ");
console.log(limite);

for(var i=0; i < limite; i++){
    c = a + b;
    a = b;
    b = c;
    process.stdout.write(c + " ");
}
*/

//2. Otra manera desarrollar el fibonacci
const prompt=require("prompt-sync")();
console.clear();
console.log("App para imprimir la secuencia de fibonacci \nDesarrollado por: Jalil Espinoza");
var a = 0, b = 1, c = 0, limite = 0;
limite = prompt("Escriba el limite de la serie Fibonacci: ");
console.log(limite);
for(;;c=a+b){  
    a = b;
    b = c;
    if(c>limite) break;
    process.stdout.write(c + " ");
}