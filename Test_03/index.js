//Incluimos la referencia para trabajar como servidor web
const http=require('http');
const hostname='127.0.0.1';
const port=3000;

//Inicializamos el servidor
const server=http.createServer(
    (req,res)=>{
        res.statusCode=200;
        res.setHeader('Content-Type','text/plain');
        var salida='<h1>Mi primera app web <br> con Nodejs</h1>';
        res.end(salida);
    }
);

//Interpolar un string, ejemplo: ${hostname} y el código ascii de la coma invertida (`) es alt+96
server.listen(port, hostname, ()=>{
    console.log(`El servidor esta corriendo en http://${hostname}:${port}/`); 
});