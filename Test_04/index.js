//Incluimos la referencia para trabajar como servidor web
const http=require('http');
const hostname='127.0.0.1';
const port=3000;

//Inicializamos el servidor
const server=http.createServer(
    (req,res)=>{
        res.statusCode=200;
        res.setHeader('Content-Type','text/html');
        var salida ='<html>';
        salida += '<head><title>App con JSON</title></head>';
        salida += '<body>';
        salida +='<h1>Mi primera app web con Nodejs</h1><br><img src="https://jalilespinoza.herokuapp.com/img/perfil/jalilEspinoza_min2_150x150px.png" alt="Imagen de Mario Bros"><br><a href="https://github.com/jalilespinoza/">Mi github</a><br><a href="https://gitlab.com/jalil.espinoza">Mi gitLab</a>'; 
        salida +='</body></html>';
        res.end(salida);
    }
);

//Interpolar un string, ejemplo: ${hostname} y el código ascii de la coma invertida (`) es alt+96
server.listen(port, hostname, ()=>{
    console.log(`El servidor esta corriendo en http://${hostname}:${port}/`); 
});