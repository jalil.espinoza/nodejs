const express=require('express');
const exphbs=require('express-handlebars');
const path=require('path');
//initialization the App 
const app = express();
//initialize the setting
app.set('port',process.env.PORT || 8080);
app.set('views',path.join(__dirname, 'views')); //Establece la ruta completa de la carpeta views
app.use(require('./routes/indexController')); //Ruta del controlador de mis aplicaciones
app.use(require('./routes/homeController'));
app.use(require('./routes/registrarController'));
app.use(require('./routes/logoutController'));
app.use(express.static(path.join(__dirname, 'public')));
app.engine('.hbs',exphbs.engine({
    defaultLayout: 'main',
    layoutDir: path.join(app.get('views'),'layouts'),
    partialsDir: path.join(app.get('views'),'partials'),
    extname:'.hbs',
}));
app.set('view engine', 'hbs');

//Raise the server (Levantamos el servidor)
app.listen(app.get('port'),()=>{
        console.log('El servidor esta corriendo en el puerto', app.get('port'));
    }
);

//Algunos cambios que se pueden hacer su falla el sistema
/*
    var exphbs = require("express-handlebars");
    app.engine("handlebars",exphbs({ defaultLayout: "main" }));

    const { engine } = require("express-handlebars");
    app.engine("handlebars", engine({ defaultLayout: "main" }));
*/